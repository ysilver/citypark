package com.example.citypark.singletons

import CalculatedCost
import HistoryReservations
import PayReturn
import PaySubscriptionReturn
import User


object DataModel{

    init {
        println("Singleton class invoked.")
    }
    var user : User? = null
    var calcCost:CalculatedCost? =null
    var payReturn: PayReturn? = null
    var historyReservations:HistoryReservations? = null
    var paySubscriptionReturn:PaySubscriptionReturn? = null
}