package com.example.citypark.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.ActionBar
import com.google.zxing.WriterException

import android.graphics.Point
import android.util.Log

import androidmads.library.qrgenearator.QRGContents

import androidmads.library.qrgenearator.QRGEncoder

import android.view.WindowManager
import android.widget.ImageView
import com.example.citypark.R
import com.example.citypark.singletons.DataModel


class QrActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.hide()
        var code =  intent.getStringExtra("code")

        var closeBtn : Button = findViewById(R.id.buttonClose)
        closeBtn.setOnClickListener{
            finish()
        }
        val qrCodeIV:ImageView = findViewById(R.id.idIVQrcode);
        // the windowmanager service.
        // the windowmanager service.
        val manager = getSystemService(WINDOW_SERVICE) as WindowManager

        // initializing a variable for default display.

        // initializing a variable for default display.
        val display = manager.defaultDisplay

        // creating a variable for point which
        // is to be displayed in QR Code.

        // creating a variable for point which
        // is to be displayed in QR Code.
        val point = Point()
        display.getSize(point)

        // getting width and
        // height of a point

        // getting width and
        // height of a point
        val width: Int = point.x
        val height: Int = point.y

        // generating dimension from width and height.

        // generating dimension from width and height.
        var dimen = if (width < height) width else height
        dimen = dimen * 3 / 4

        // setting this dimensions inside our qr code
        // encoder to generate our qr code.

        // setting this dimensions inside our qr code
        // encoder to generate our qr code.
       val  qrgEncoder = QRGEncoder(code, null, QRGContents.Type.TEXT, dimen)
        try {
            // getting our qrcode in the form of bitmap.
            val bitmap = qrgEncoder.encodeAsBitmap()
            // the bitmap is set inside our image
            // view using .setimagebitmap method.
            qrCodeIV.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            // this method is called for
            // exception handling.
            Log.e("Tag", e.toString())
        }
    }
}