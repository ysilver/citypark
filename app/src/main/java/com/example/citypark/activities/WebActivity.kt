package com.example.citypark.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import androidx.appcompat.app.ActionBar
import com.example.citypark.R
import com.example.citypark.singletons.DataModel

class WebActivity : AppCompatActivity() {
    var url:String = ""
    var order:Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        val actionBar: ActionBar? = supportActionBar
        actionBar?.hide()

        var closeBtn: Button = findViewById(R.id.closeWebViewButton)
        closeBtn.setOnClickListener{
            if (order == 1) {
                startActivity(Intent(this, OrderBookParkActivity::class.java))
            }
            else{
                startActivity(Intent(this, OrderPermenantParkActivitty::class.java))
            }
        }
        var webView :WebView = findViewById(R.id.webView)
        webView = findViewById(R.id.webView)
        webView.settings.setJavaScriptEnabled(true)
        var url =  intent.getStringExtra("url")
        order = intent.getIntExtra("order",0)

        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url != null) {
                    view?.loadUrl(url)
                }
                return true
            }
        }
        if (url != null) {
            webView.loadUrl(url)
        }
    }
}
