package com.example.citypark.activities

import CalculatedCost
import PayReturn
import android.app.DatePickerDialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import com.example.citypark.network.ApiInterface
import com.example.citypark.singletons.DataModel
import com.google.android.material.button.MaterialButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import android.app.TimePickerDialog
import android.content.Intent
import android.widget.*
import com.example.citypark.R
import com.example.citypark.models.PayReservation
import java.text.SimpleDateFormat
import java.util.*


class BookParkActivity : AppCompatActivity() {
            var dateText :EditText? = null
            var cal = Calendar.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_park)
        val arrivalTime: EditText? = findViewById(R.id.arrivalTimeText)
        val durationTime: EditText = findViewById(R.id.durationTime)

        var payButton: MaterialButton = findViewById(R.id.buttonPayReservation)

        payButton.setOnClickListener {
            val joinedDate = "${dateText?.text.toString()} ${arrivalTime?.text.toString()}"
            val durperiod = durationTime.text.toString()
            val licensceno :EditText =  findViewById(R.id.licno)
            val payReservation = PayReservation(joinedDate,durperiod,licensceno.text.toString())
            val SHARED_PREF = "sharedPreferences"
            val sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
            val api_token = sharedPreferences.getString("api_token", "noapitoken")
            val apiInterface = ApiInterface.create()
                .payReservation("Bearer ${api_token.toString()}", payReservation)
            apiInterface.enqueue(object : Callback<PayReturn> {
                override fun onResponse(
                    call: Call<PayReturn>?,
                    response: Response<PayReturn>?
                ) {

                    if (response?.body() != null) {
                        val json = response.body()
                        // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                        System.out.println(json.toString())
                        if (json != null) {

                            DataModel.payReturn = json


                        }
                        val intent = Intent(application, WebActivity::class.java)
                        val url = json?.checkout_url
                        intent.putExtra ("url",url)
                        intent.putExtra("order",1)
                        startActivity(intent)                    }
                }

                override fun onFailure(call: Call<PayReturn>?, t: Throwable?) {
                    Toast.makeText(application, "something went wrong" + t, Toast.LENGTH_SHORT)
                        .show()
                }
            })


        }


        var closeButton: Button = findViewById(R.id.closeButton)

       dateText = findViewById(R.id.dateText)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.hide()
        closeButton.setOnClickListener {
            finish()
        }


        var calcCost: MaterialButton = findViewById(R.id.buttonCalcCost)
        calcCost.setOnClickListener {

          //  val time= Date(arrivalTime?.text.toString())
            val sdf =SimpleDateFormat("hh:mm")
            val fixedtime = arrivalTime?.text.toString()
            val joinedDate = "${dateText?.text.toString()} ${fixedtime}"
            val durperiod = durationTime.text.toString()
            val SHARED_PREF = "sharedPreferences"
            val sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
            val api_token = sharedPreferences.getString("api_token", "noapitoken")
            val apiInterface = ApiInterface.create()
                .calculateCost("Bearer ${api_token.toString()}", joinedDate, durperiod)
            apiInterface.enqueue(object : Callback<CalculatedCost> {
                override fun onResponse(
                    call: Call<CalculatedCost>?,
                    response: Response<CalculatedCost>?
                ) {

                    if (response?.body() != null) {
                        val json = response.body()
                        // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                        System.out.println(json.toString())
                        if (json != null) {
                            Toast.makeText(
                                application,
                                "successfully connected",
                                Toast.LENGTH_SHORT
                            )
                            DataModel.calcCost = json
                            var costTxt: TextView = findViewById(R.id.cost)
                            costTxt.setText("${json.charge.toString()} ευρώ")
                        }
                    }
                }

                override fun onFailure(call: Call<CalculatedCost>?, t: Throwable?) {
                    Toast.makeText(application, "something went wrong" + t, Toast.LENGTH_SHORT)
                        .show()
                }
            })
        }


        arrivalTime?.setOnClickListener {


            val timePickerDialog = TimePickerDialog(
                this@BookParkActivity,
                { timePicker, hourOfDay, minutes -> arrivalTime.setText("${hourOfDay}:${minutes}") },
                0,
                0,
                true

            )

            timePickerDialog.show()

        }

        val dateSetListener = object :DatePickerDialog.OnDateSetListener{
            override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                    cal.set(Calendar.YEAR,year)
                    cal.set(Calendar.MONTH,month)
                    cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                    updateDateInView()
            }
        }
        val dateText: EditText = findViewById(R.id.dateText)
        dateText.setOnClickListener {

           val datePickerDialog = DatePickerDialog(this@BookParkActivity,dateSetListener,
                                            cal.get(Calendar.YEAR),
                                            cal.get(Calendar.MONTH),
                                            cal.get(Calendar.DAY_OF_MONTH))
            datePickerDialog.show()
        }


    }
    private fun updateDateInView(){
        val myFormat = "yyyy-MM-dd"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        dateText?.setText("${sdf.format(cal.time)}")    }
}