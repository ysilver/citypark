package com.example.citypark.activities

import HistoryReservations
import PayReturn
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.content.Intent
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import com.example.citypark.R
import com.example.citypark.network.ApiInterface
import com.example.citypark.singletons.DataModel
import com.google.android.material.button.MaterialButton
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class OrderBookParkActivity : AppCompatActivity() {
    var ammountDeposited :TextView? = null
    var status : TextView? =null
    var qrBtn:Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_book_park)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.hide()
       checkStatusAndButton()
        val arrival :TextView =findViewById(R.id.arrivalTime)
        val date: TextView =findViewById(R.id.datereservation)
        val duration: TextView = findViewById(R.id.duration)
        val plate:TextView =findViewById(R.id.plateno)
        ammountDeposited =  findViewById(R.id.amountPayed)
        status = findViewById(R.id.status)
        qrBtn = findViewById(R.id.QRButton)
        //ammountDeposited.setText("${DataModel.payReturn!!.charge}")
        plate.setText("${DataModel.payReturn!!.plate}")

        val arrivalDate = DataModel.payReturn!!.begin.subSequence(0,10)
        val arrivatTime = DataModel.payReturn!!.begin.subSequence(11,16)
        arrival.setText(arrivatTime)
        date.setText(arrivalDate)
      var closeBtn: Button = findViewById(R.id.close)

        closeBtn.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) // Removes other Activities from stack
          //  val intent = Intent(application, WebActivity::class.java)

          //  intent.putExtra("code",DataModel.payReturn?.code)

            startActivity(intent)
        }

        if(DataModel.payReturn!!.status == "valid"){
        status!!.setText("Εξοφλημένο")
        ammountDeposited!!.setText("${DataModel.payReturn!!.charge} ευρώ")}
        else if(DataModel.payReturn!!.status =="unpaid"){
            status!!.setText("Σε εξέλιξη")
        }
        else{
            status!!.setText("Αποτυχία")
            qrBtn!!.isClickable = false
        }


        var refreshButton:MaterialButton =  findViewById(R.id.refreshButton)
        refreshButton.setOnClickListener{

           checkStatusAndButton()

        }


        qrBtn?.setOnClickListener{
            val intent = Intent(this, QrActivity::class.java)
         //   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) // Removes other Activities from stack
            //  val intent = Intent(application, WebActivity::class.java)

             intent.putExtra("code",DataModel.payReturn?.code)

            startActivity(intent)
        }

    }
    fun checkStatusAndButton(){
        val SHARED_PREF = "sharedPreferences"
        val sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
        val api_token = sharedPreferences.getString("api_token", "noapitoken")
        val apiInterface = ApiInterface.create().getBookingByCode("Bearer ${api_token.toString()}",DataModel.payReturn!!.code)
        apiInterface.enqueue(object : Callback<PayReturn> {
            override fun onResponse(call: Call<PayReturn>?, response: Response<PayReturn>?) {

                if (response?.body() != null) {
                    val json = response.body()
                    // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                    System.out.println(json.toString())
                    if (json != null) {
                        Toast.makeText(application,"successfully connected", Toast.LENGTH_SHORT)

                        if (json.status == "unpaid"){
                            status?.setText("Σε εξέλιξη")
                        }
                        else if(json.status == "valid"){
                            status?.setText("Εξοφλημένο")
                            ammountDeposited?.setText("${json.charge} ευρώ")}

                    }
                    else{
                        status?.setText("Αποτυχία πληρωμής")
                        qrBtn?.isClickable =false

                    }
                }
            }

            override fun onFailure(call: Call<PayReturn>?, t: Throwable?) {
                Toast.makeText(application, "something went wrong" + t, Toast.LENGTH_SHORT).show()
            }
        })

    }
}