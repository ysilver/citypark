package com.example.citypark.activities

import HistoryReservations
import PayReturn
import User
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.widget.Button
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.example.citypark.network.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.widget.Toast
import com.example.citypark.R
import com.example.citypark.models.BodyRegisterUser
import com.example.citypark.singletons.DataModel
import com.google.android.material.button.MaterialButton
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class MainActivity : AppCompatActivity() {
    @SuppressLint("HardwareIds")
    var loadSequence:Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val SHARED_PREF = "sharedPreferences"
        val sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
        val api_token = sharedPreferences.getString("api_token", "noapitoken")
        val permBtn:Button =findViewById(R.id.buttonMonthly)
        var reserveButton: Button =  findViewById(R.id.buttonReserve)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.hide()

        val apiInterface = ApiInterface.create().getHistoryBookings("Bearer ${api_token.toString()}")
        apiInterface.enqueue(object : Callback<HistoryReservations> {
            override fun onResponse(call: Call<HistoryReservations>?, response: Response<HistoryReservations>?) {

                if (response?.body() != null) {
                    val json = response.body()
                    // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                    System.out.println(json.toString())
                    if (json != null) {
                        Toast.makeText(application,"successfully connected",Toast.LENGTH_SHORT)
                        DataModel.historyReservations = json
                        val date =  DataModel.historyReservations!!.data[0].end
                        val wantedDate = date.subSequence(0,10)
                      //  var datewanted = LocalDate.parse("${wantedDate}")
                        val currentDate = Date()

                        var data =  json.data[0]
                        if (data != null) {
                            DataModel.payReturn = PayReturn(
                                data.status,
                                data.begin,
                                data.end,
                                data.plate,
                                data.user_id,
                                data.charge,
                                data.code,
                                data.updated_at,
                                data.created_at,
                                data.id,
                                ""
                            )
                        }
                            if (SimpleDateFormat("yyyy-MM-dd").parse(date).after( Date())) {

                            loadSequence = true


                        }
                    }
                }
            }

            override fun onFailure(call: Call<HistoryReservations>?, t: Throwable?) {
                Toast.makeText(application, "something went wrong" + t, Toast.LENGTH_SHORT).show()
            }
        })




             reserveButton.setOnClickListener {
                 if (loadSequence == true) {
                     startActivity(Intent(this, OrderBookParkActivity::class.java))

                 } else {
                     startActivity(Intent(this, BookParkActivity::class.java))

                 }
             }

        permBtn.setOnClickListener{
            startActivity(Intent(this, PermenantParkActivity::class.java))
        }

        var android_id:String = Settings.Secure.getString(this.contentResolver,
            Settings.Secure.ANDROID_ID);
        val identification = android_id
        val deviceName = android.os.Build.MODEL

        if (sharedPreferences!!.getBoolean("my_first_time", true)) {
            //the app is being launched for first time, do something
            //Log.d("Comments", "First time")

            // first time task



        val bodyRegisterUser :BodyRegisterUser = BodyRegisterUser(identification,deviceName)
        val apiInterface = ApiInterface.create().registerUser(bodyRegisterUser)
        apiInterface.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>?, response: Response<User>?) {

                if (response?.body() != null) {
                    val json = response.body()
                    // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                    System.out.println(json.toString())
                    if (json != null) {
                        Toast.makeText(application,"successfully connected",Toast.LENGTH_SHORT)
                        DataModel.user = json
                        val token_key = sharedPreferences.edit()
                        token_key.putString("api_token",json.access_token)
                        token_key.commit()

                    }
                }
            }

            override fun onFailure(call: Call<User>?, t: Throwable?) {
                Toast.makeText(application, "something went wrong" + t, Toast.LENGTH_SHORT).show()
            }
        })
            // record the fact that the app has been started at least once
            sharedPreferences.edit().putBoolean("my_first_time", false).commit()
        }
    }
}