package com.example.citypark.activities

import PaySubscriptionReturn
import android.app.DatePickerDialog
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import com.example.citypark.R
import com.example.citypark.models.PaySubscription
import com.example.citypark.network.ApiInterface
import com.example.citypark.singletons.DataModel
import com.google.android.material.button.MaterialButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class PermenantParkActivity : AppCompatActivity() {
    var cal = Calendar.getInstance()
    var dateText: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permenant_park)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.hide()
        val licenceNo :EditText  =findViewById(R.id.licenceNo)
        dateText = findViewById(R.id.startDate)
        val closeBtn: Button = findViewById(R.id.closeButton)
        closeBtn.setOnClickListener {
            finish()
        }
        val payButton: MaterialButton = findViewById(R.id.buttonPay)
        payButton.setOnClickListener {
            val SHARED_PREF = "sharedPreferences"
            val sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
            val api_token = sharedPreferences.getString("api_token", "noapitoken")
            val paySubsc = PaySubscription(dateText?.text.toString(),licenceNo.text.toString())

            val apiInterface =
                ApiInterface.create().paySubscription("Bearer ${api_token.toString()}",paySubsc)
            apiInterface.enqueue(object : Callback<PaySubscriptionReturn> {
                override fun onResponse(
                    call: Call<PaySubscriptionReturn>?,
                    response: Response<PaySubscriptionReturn>?
                ) {

                    if (response?.body() != null) {
                        val json = response.body()
                        // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                        System.out.println(json.toString())
                        if (json != null) {
                            Toast.makeText(
                                application,
                                "successfully connected",
                                Toast.LENGTH_SHORT
                            )
                            DataModel.paySubscriptionReturn = json
                            val intent = Intent(application, WebActivity::class.java)
                            val url = json.checkout_url
                            intent.putExtra ("url",url)
                            intent.putExtra("order",2)
                           startActivity(intent)
                          //  startActivity(Intent(application, WebActivity::class.java))

                        }
                    }
                }

                override fun onFailure(call: Call<PaySubscriptionReturn>?, t: Throwable?) {
                    Toast.makeText(application, "something went wrong" + t, Toast.LENGTH_SHORT)
                        .show()
                }
            })

        }
        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, month)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        }
        dateText?.setOnClickListener {

            val datePickerDialog = DatePickerDialog(
                this@PermenantParkActivity, dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )
            datePickerDialog.show()
        }


    }

    private fun updateDateInView() {
        val myFormat = "yyyy-MM-dd"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        dateText?.setText("${sdf.format(cal.time)}")
    }

}
