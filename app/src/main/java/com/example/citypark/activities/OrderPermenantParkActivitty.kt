package com.example.citypark.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.ActionBar
import com.example.citypark.R
import com.example.citypark.singletons.DataModel
import com.google.android.material.button.MaterialButton

class OrderPermenantParkActivitty : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_permenant_park_activitty)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.hide()
       val closeBtn: Button = findViewById(R.id.closeButton)
        closeBtn.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) // Removes other Activities from stack
            //  val intent = Intent(application, WebActivity::class.java)

            //  intent.putExtra("code",DataModel.payReturn?.code)

            startActivity(intent)
        }
        val qrBtn:MaterialButton = findViewById(R.id.QRButton)
        qrBtn.setOnClickListener{
            val intent = Intent(application, QrActivity::class.java)
            val code = DataModel.paySubscriptionReturn?.code
            intent.putExtra ("code",code)
           // intent.putExtra("order",2)
            startActivity(intent)
        }
    }
}