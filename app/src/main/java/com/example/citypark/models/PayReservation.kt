package com.example.citypark.models

data  class PayReservation (

        val begin :String,
        val duration_hours :String,
        val plate:String
        )
