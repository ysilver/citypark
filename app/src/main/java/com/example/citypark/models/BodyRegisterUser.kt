package com.example.citypark.models


import com.google.gson.annotations.SerializedName

data class BodyRegisterUser (

     val identifier :String,
    val device_name :String,

)
