package com.example.citypark.network

import CalculatedCost
import HistoryReservations
import PayReturn
import PaySubscriptionReturn
import User
import com.example.citypark.models.BodyRegisterUser
import com.example.citypark.models.PayReservation
import com.example.citypark.models.PaySubscription
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {

    @POST("api/users/register-visitor")
    fun registerUser(@Body bodyRegisterUser: BodyRegisterUser/*,@Query(value="api_token",encoded = false)api_token:String*/): Call<User>

    @GET("api/bookings/charge")
    fun calculateCost(  @Header ("Authorization:")apiToken:String ,@Query (value="begin",encoded = false)begin: String, @Query(value="duration_hours",encoded =false)duration_hour:String): Call<CalculatedCost>

    @POST("api/bookings")
    fun payReservation( @Header ("Authorization:")apiToken:String,@Body register: PayReservation): Call<PayReturn>

    @GET("api/bookings")
    fun getHistoryBookings( @Header ("Authorization:")apiToken:String): Call<HistoryReservations>

    @GET("api/bookings/{booking_code}")
    fun getBookingByCode( @Header ("Authorization:")apiToken:String,@Path ("booking_code")code:String): Call<PayReturn>

    @POST("api/subscriptions")
    fun paySubscription( @Header ("Authorization:")apiToken:String,@Body paySubscription:  PaySubscription): Call<PaySubscriptionReturn>
    //    @POST("api/clients/verify/{mobile}")
//    fun verifyPhone(@Body verify: VerifyPhone, @Path(value="mobile",encoded = false)mobile:String?): Call<VerifyReturn>
    companion object {

        var BASE_URL = "http://185.4.134.182/"

        fun create() : ApiInterface {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiInterface::class.java)

        }
    }
}